import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import java.io.InputStream
import java.lang.Exception
import java.lang.StringBuilder
import java.nio.charset.StandardCharsets

class RootHandler : HttpHandler {
    override fun handle(t: HttpExchange?) {
        if(t != null) {
            var errCode = 200
            val resp: String = try {
                this::class.java.classLoader.getResource("index.html").readText()
            }catch (ex: Exception) {
                errCode = 500
                ex.printStackTrace()
                ex.message?:"Tomb of the Unknown Error"
            }

            t.sendResponseHeaders(errCode, resp.length.toLong())
            println("RootHandler: ${t.requestMethod} ${t.requestURI.path}")
            val os = t.responseBody
            os?.write(resp.toByteArray())
            os?.close()
        }
    }

}