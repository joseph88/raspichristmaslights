data class PwmPattern (val name: String, val list: List<PwmFrame>){
    fun go() = animate(list)
}