import com.pi4j.io.gpio.*
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import com.sun.net.httpserver.HttpServer
import java.net.*
import java.util.*

val gpio = GpioFactory.getInstance()
val red = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_02)
val yellow = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_03)
val blue = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_04)
val orange = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_05)
val green = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_06)
val btnChange = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, PinPullResistance.PULL_UP)

var go = true
var switch = false

fun main(args: Array<String>) {
    println("PWM Program Start")
    var downAt = Long.MAX_VALUE
    var dblDownAt = downAt - 10000L
    var downDiff = 10000L

    btnChange.setShutdownOptions(true)
    btnChange.setDebounce(30)
    btnChange.addListener(GpioPinListenerDigital {
            //println("Button Change: ${it.pin} : ${it.state}")
        if(it.state == PinState.HIGH) {
            Patterns.currIdx++
            dblDownAt = downAt
            downAt = Long.MAX_VALUE
            if (downDiff < 260){
                Patterns.currPattern = "off"
                downDiff = 10000L
            }
            switch = true
        } else {
            downAt = Calendar.getInstance().timeInMillis
            downDiff = downAt - dblDownAt
        }
    })

    val server = HttpServer.create(InetSocketAddress(80), 0)
    server.createContext("/api", ApiHandler())
    server.createContext("/", RootHandler())
    server.executor = null
    server.start()


    while(go){
        Patterns.go()
        switch = false
        if(Calendar.getInstance().timeInMillis - downAt > 5000) go = false
    }

    server.stop(0)
    shutdown()
    println("PWM Program end")
    if(!go) Runtime.getRuntime().exec("shutdown now")
}

fun sw(pattern: String? = null) {
    if(pattern != null) Patterns.currPattern = pattern
    else Patterns.currIdx++
    println("${Patterns.currIdx}: ${Patterns.currPattern}")
    switch = true
}

fun animate(list: List<PwmFrame>) {
    for(k in 0 until list.size){
        red.pwm = list[k].red
        yellow.pwm = list[k].yellow
        blue.pwm = list[k].blue
        orange.pwm = list[k].orange
        green.pwm = (list[k].green * 0.88).toInt()
        Thread.sleep(50)
        if(!go || switch) return
        //println("${red.pwm} ${orange.pwm} ${yellow.pwm} ${green.pwm} ${blue.pwm}")
    }
}

fun shutdown() {
    red.pwm = 0
    yellow.pwm = 0
    blue.pwm = 0
    orange.pwm = 0
    green.pwm = 0
    gpio.shutdown()
}