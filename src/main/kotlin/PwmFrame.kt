class PwmFrame(var red: Int = 0,
               var orange: Int = 0,
               var yellow: Int = 0,
               var green: Int = 0,
               var blue: Int = 0) {

    companion object {
        val curveLinearUp = mutableListOf<Int>().apply {
            for(i in 1..96) { this += i }
        }
        val curveLinearDown = mutableListOf<Int>().apply {
            for(i in 96 downTo 1) { this += i }
        }
        val curveLinearZero = mutableListOf<Int>().apply {
            this += 0
            addAll(curveLinearUp)
            this += 96
            addAll(curveLinearDown)
            this += 0
        }.toList()

        val curveLinearNZ = mutableListOf<Int>().apply {
            this += 1
            addAll(curveLinearUp)
            this += 96
            addAll(curveLinearDown)
            this += 1
        }.toList()

        val curveLinearNZHoles10 = genCurveLinearHoles(10)
        val curveLinearNZHoles20 = genCurveLinearHoles(20)
        val curveLinearNZHoles40 = genCurveLinearHoles(40)
        val curveLinearNZHoles80 = genCurveLinearHoles(80)
        val curveLinearNZHoles120 = genCurveLinearHoles(120)

        val curveSUp = listOf (
                1, 1, 1, 1, 1, 1, 2, 2, 2, 2, //0-9
                2, 2, 2, 3, 3, 3, 3, 3, 3, 3, //10-19
                3, 4, 4, 4, 4, 4, 5, 5, 5, 5, //20-29
                5, 6, 6, 6, 6, 7, 7, 7, 8, 8, //30-39
                8, 9, 9, 9, 10, 10, 11, 11, 12, 12, //40-49
                13, 13, 14, 14, 15, 15, 16, 17, 18, 18, //50-59
                19, 20, 21, 22, 23, 24, 25, 26, 27, 28, //60-69
                29, 31, 32, 33, 35, 36, 38, 39, 41, 43, //70-79
                45, 47, 49, 51, 53, 55, 58, 60, 63, 66, //80-89
                68, 71, 74, 78, 81, 85, 88, 92, 96  //90-98
        )
        val curveSDown = curveSUp.reversed()

        val curveS = mutableListOf<Int>().apply {
            addAll(curveSUp)
            this += 96
            addAll(curveSDown)
            this += 0
        }.toList()

        val curveSNZ = mutableListOf<Int>().apply {
            addAll(curveSUp)
            this += 96
            addAll(curveSDown)
            this += 1
        }.toList()

        val curveSHoles5 = genCurveSHoles(5)
        val curveSHoles10 = genCurveSHoles(10)
        val curveSHoles35 = genCurveSHoles(35)
        val curveSHoles50 = genCurveSHoles(50)
        val curveSHoles75 = genCurveSHoles(75)
        val curveSHoles100 = genCurveSHoles(100)
        val curveSHoles150 = genCurveSHoles(150)

        fun genCurveSHoles(sz: Int) : List<Int> =
            mutableListOf<Int>().apply {
                for (i in 0..sz) this += 0
                addAll(curveSUp)
                for (i in 0..sz) this += 90
                addAll(curveSDown)
            }.toList()

        fun genCurveLinearHoles(sz: Int) : List<Int> =
                mutableListOf<Int>().apply {
                    for (i in 0..sz) this += 2
                    addAll(curveLinearUp)
                    for (i in 0..sz) this += 90
                    addAll(curveLinearDown)
                }.toList()
    }
}