import PwmFrame.Companion.curveSNZ
import java.net.Inet6Address
import java.net.NetworkInterface
import java.net.SocketException

const val ON = 85
const val OFF = 0
const val MAX = 99
const val HALF = 20

object Patterns {
    val list = listOf(
        PwmPattern("ipaddr", genIpAddressPattern()),
        PwmPattern("off", listOf(PwmFrame())),
        PwmPattern("half",listOf(PwmFrame(HALF, HALF, HALF, HALF, HALF))),
        PwmPattern("on", listOf(PwmFrame(ON, ON, ON, ON, ON))),
        PwmPattern("twinkle", genTwinkle(8)),
        PwmPattern("slowTwinkle", genTwinkle(1)),
        PwmPattern("redGreen", genRedGreen()),
        PwmPattern("chase", genChase(2)),
        PwmPattern("slowChase", fadeChase(1).toList()),
        PwmPattern("medChase", fadeChase(10).toList()),
        PwmPattern("fastChase", fadeChase(30).toList()),
        PwmPattern("sparkle", genSparkle()),
        PwmPattern("warmCool", genWarmCool()),
        PwmPattern("rgSolid", listOf(PwmFrame(red=ON, green=ON))),
        PwmPattern("red", listOf(PwmFrame(red=ON))),
        PwmPattern("orange", listOf(PwmFrame(orange=ON))),
        PwmPattern("yellow", listOf(PwmFrame(yellow=ON))),
        PwmPattern("green", listOf(PwmFrame(green=75))),
        PwmPattern("blue", listOf(PwmFrame(blue=ON)))
    )

    private var ci: Int = 0
    var currIdx: Int
        get() = ci
        set(value) {
            if(value <= 0) ci = list.size-1
            else if(value >= list.size ) ci = 1
            else ci = value
        }
    var currPattern: String
        get() = list[ci].name
        set(value) {
            val ni = list.indexOfFirst { value == it.name }
            if(ni >= 0) ci = ni
        }
    val nameList = list.map { it.name }

    /*************************************************
     *  Helper Methods
     */
    fun go() = list[ci].go()

    /******************************************************
     *  Pattern Functions
     */
    private fun fadeChase(stp: Int) : List<PwmFrame> {
        val crv = PwmFrame.curveS
        val cnt = PwmFrame.curveS.count()-1
        return mutableListOf<PwmFrame>().apply {
            for(i in 0..cnt step stp) {
                add(PwmFrame(red = crv[i]))
            }
            for(i in 0..cnt step stp) {
                add(PwmFrame(green = crv[i]))
            }
            for(i in 0..cnt step stp) {
                add(PwmFrame(orange = crv[i]))
            }
            for(i in 0..cnt step stp) {
                add(PwmFrame(blue = crv[i]))
            }
            for(i in 0..cnt step stp) {
                add(PwmFrame(yellow = crv[i]))
            }
        }
    }

    private fun genChase(stp: Int) : List<PwmFrame> = mutableListOf<PwmFrame>().apply {
        for(i in 0..stp) add(PwmFrame(red = ON))
        for(i in 0..stp) add(PwmFrame(orange = ON))
        for(i in 0..stp) add(PwmFrame(yellow = ON))
        for(i in 0..stp) add(PwmFrame(green = ON))
        for(i in 0..stp) add(PwmFrame(blue = ON))
    }.toList()


    private fun genTwinkle(stp: Int) : List<PwmFrame> = mutableListOf<PwmFrame>().apply {
        val crv = if(stp > 3) PwmFrame.curveLinearNZHoles40 else PwmFrame.curveLinearNZHoles120
        val cnt = crv.count()-1
        val p1 = cnt/5
        val p2 = p1*2
        val p3 = p1*3
        val p4 = p1*4
        for(i in 0..cnt step stp) {
            add(PwmFrame(red=crv[i], green=crv[(i+p1)%cnt], orange=crv[(i+p2)%cnt], blue=crv[(i+p3)%cnt], yellow=crv[(i+p4)%cnt]))
        }
    }.toList()

    private fun genSparkle(): List<PwmFrame> = mutableListOf<PwmFrame>().apply {
        for(i in 0..5) {
            add(PwmFrame(red = ON))
            add(PwmFrame(green = ON))
            add(PwmFrame(orange = ON))
            add(PwmFrame(blue = ON))
            add(PwmFrame(yellow = ON))
        }
    }.toList()

    private fun genWarmCool(stp: Int = 2) : List<PwmFrame> = mutableListOf<PwmFrame>().apply {
        val crvu = PwmFrame.curveSUp
        val cntu = crvu.count()-1
        val crvd = PwmFrame.curveSDown
        val cntd = crvd.count()-1
        for(i in 0..cntu step stp) {
            add(PwmFrame(red = crvu[i], orange =  crvu[kotlin.math.min((i * 1.2).toInt(), cntu)], yellow =  crvu[kotlin.math.min((i * 1.4).toInt(), cntu)]))
        }
        for(i in 0..cntd step stp) {
            if(cntd - i > 10) add(PwmFrame(red = crvd[i], orange =  crvd[kotlin.math.min((i * 1.2).toInt(), cntd)], yellow =  crvd[kotlin.math.min((i * 1.4).toInt(), cntd)]))
            else add(PwmFrame(red = crvd[i], orange =  crvd[kotlin.math.min((i * 1.2).toInt(), cntd)],
                    yellow =  crvd[kotlin.math.min((i * 1.4).toInt(), cntd)], green = 1, blue = 1))
        }
        for(i in 0..cntu step stp) {
            add(PwmFrame(green = crvu[i], blue =  crvu[kotlin.math.min((i * 1.4).toInt(), cntu)]))
        }
        for(i in 0..cntd step stp) {
            if(cntd - i > 10) add(PwmFrame(green = crvd[i], blue =  crvd[kotlin.math.min((i * 1.4).toInt(), cntd)]))
            else add(PwmFrame(red=1, orange=1, yellow = 1, green = crvd[i], blue =  crvd[kotlin.math.min((i * 1.4).toInt(), cntd)]))
        }
    }.toList()

    private fun genRedGreen(stp: Int = 4) : List<PwmFrame> = mutableListOf<PwmFrame>().apply {
        val crv = curveSNZ
        val cnt = curveSNZ.size-1
        val ofs = cnt/2
        for(i in 0..cnt step stp) {
            add(PwmFrame(red = crv[i], green = crv[(i+ofs) % cnt]))
        }
    }.toList()

    private fun genIpAddressPattern() : List<PwmFrame> = mutableListOf<PwmFrame>().apply {
        var ip: String
        try {
            val interfaces = NetworkInterface.getNetworkInterfaces()
            while (interfaces.hasMoreElements()) {
                val iface = interfaces.nextElement()
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback || !iface.isUp)
                    continue
                val addresses = iface.inetAddresses
                while (addresses.hasMoreElements()) {
                    val addr = addresses.nextElement()
                    if (addr is Inet6Address) continue
                    ip = addr.hostAddress
                    //println(iface.getDisplayName() + " " + ip)
                    val sp = ip.split('.')
                    if(sp.size > 3){
                        val fo = sp[3].toInt()
                        val h = fo/100
                        val t = (fo - h * 100)/10
                        val o = (fo - h * 100 - t * 10)
                        println("$h :: $t :: $o")
                        for(red in 0..10) {
                            if(h > red) {
                                for (j in 1..6) add(PwmFrame(red = ON))
                                for (k in 1..3) add(PwmFrame())
                            }
                        }
                        for(orng in 0..10) {
                            if(t > orng) {
                                for (j in 1..6) add(PwmFrame(orange = ON))
                                for (k in 1..3) add(PwmFrame())
                            }
                        }
                        for(yelw in 0..10) {
                            if(o > yelw) {
                                for (j in 1..6) add(PwmFrame(yellow = ON))
                                for (k in 1..3) add(PwmFrame())
                            }
                        }
                    }
                }
            }
        } catch (e: SocketException) {
            throw RuntimeException(e)
        }
    }.toList()
}