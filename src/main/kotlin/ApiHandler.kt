import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler

class ApiHandler : HttpHandler {
    override fun handle(t: HttpExchange?) {
        if(t != null) {
            val path = t.requestURI.path.split('/').drop(2)
            var err: String? = null
            var code = 200

            val resp:String? = if(path.isNotEmpty()) {
                when (path[0]) {
                    "switch" -> doSwitch(path.drop(1), t)
                    "pattern" -> doPattern()
                    else -> {
                        code = 404
                        err = "Not Found /api/${path[0]}"
                        null
                    }
                }
            } else {
                err = "Invalid path"
                code = 404
                null
            }

            val retString = resp?:"{\"error\":\"$err\"}"
            t.sendResponseHeaders(code, retString.length.toLong())
            println("ApiHandler: ${t.requestMethod} ${t.requestURI.path}")
            val os = t.responseBody
            os?.write(retString.toByteArray())
            os?.close()
        }
    }


    private fun doSwitch(list: List<String>, t: HttpExchange?) : String {
        //println("Switching")
        if(list.isEmpty()) sw()
        else sw(list[0])
        return Patterns.currPattern
    }

    private fun doPattern() : String = Patterns.currPattern
}